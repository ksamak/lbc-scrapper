#!/usr/bin/env python3

import urllib.request, urllib.parse, urllib.error
import requests
import re
import sqlite3
import os
import argparse
import gettext
import logging
import logging.handlers
from datetime import datetime
from bs4 import BeautifulSoup as bs

logger = logging.getLogger("default")
logger.setLevel(logging.INFO)

formatter = logging.Formatter("%(asctime)s | %(levelname)8s | %(message)s")

stStdout = logging.StreamHandler()
stStdout.setFormatter(formatter)
logger.addHandler(stStdout)

lbcaDir = os.environ['HOME']+"/.lbca"
logsDir = lbcaDir + "/logs"
if not os.path.isdir(logsDir):
    if not os.path.isdir(lbcaDir):
        os.mkdir(lbcaDir)
    os.mkdir(logsDir)

stLogfile = logging.handlers.RotatingFileHandler(logsDir+'/log', maxBytes=256*1024, backupCount=10)
stLogfile.setFormatter(formatter)
#stLogfile.doRollover()
logger.addHandler(stLogfile)

parser = argparse.ArgumentParser(description="Le bon coin alert generator")

parser.add_argument("-s", metavar="search", dest="searches", action="append", help="Searches to perform")
parser.add_argument("--exclude-title", metavar="excludeTitle", dest="excludeTitle", action="append", help="Keywords that should not be in announce title")
parser.add_argument("--exclude-contents", metavar="excludeContents", dest="excludeContents", action="append", help="Keywords that should not be in anounce content")
parser.add_argument("-r", metavar="region", dest="region", help="Region", default="")
parser.add_argument("-a --request-arg", dest="request_args", action="append", default=[], help="additional request arguments")

parser.add_argument("-e", metavar="email-to", dest="email_to", help="Email results to", default=None)
parser.add_argument("-f", metavar="email-from", dest="email_from", help="Email from", default=None)
parser.add_argument("-u", metavar="email-subject", dest="email_subject", help="Email's subject", default="")
parser.add_argument("--serv --smtp-server", metavar="smtp-server", dest="server", help="SMTP server to use", default="localhost")
parser.add_argument("-p", metavar="smtp-port", dest="port", help="SMTP port", default=465)
parser.add_argument("--login --smtp-login", metavar="smtp-login", dest="smtpLogin", help="SMTP login name eg. troy@example.net", default="")
parser.add_argument("--pass --smtp-pass", metavar="smtp-pass", dest="smtpPass", help="SMTP password", default="")

parser.add_argument("--gui", dest="gui", action="store_true", help="Use WxWidget GUI")
parser.add_argument("--database", dest="db", action="store", default="db", help="specify database name to use (in $HOME/.lbca/)")
parser.add_argument("-v", dest="verbose", action="store_true", help="verbosity")

args = parser.parse_args()

# Headers
#headers = {'User-Agent': 'Mozilla/5.0'}
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'}

# LBC specific
baseUrl = "https://www.leboncoin.fr/recherche/?"
urlSort = "&sort=time&order=asc"
#linkRegex = re.compile("https://www.leboncoin.fr/[a-z0-9]+/[0-9]{8,12}\\.html")

#recherche/?text=ebike&locations=Poitiers_86000
#https://www.leboncoin.fr/recherche/?text=batterie&locations=Poitiers_86000__46.58387_0.3357_8948

import unicodedata
def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return u"".join([c.lower() for c in nfkd_form if not unicodedata.combining(c)])

class SearchLink():
    def __init__(self, url, title, contents=""):
        self.url = url
        self.title = remove_accents(title)
        self.contents = contents

    def __str__(self):
        return "title: {title}  {url}".format(title=self.title, url=self.url)

    def matches(self, args):
        if args.excludeTitle:
            for word in args.excludeTitle:
                if word in self.title:
                    return False
        if args.excludeContents:
            for word in args.excludeContents:
                if word in self.contents:
                    return False
        return True

# Requests handling
def searchToLinks(search):
    logger.info("Searching for {search}".format(search=search))
    links = []
    request_arguments = {"text": search, "location": args.region, "sort": "time", "order":"asc"}
    for arg in args.request_args:
        arg = arg.split("=")
        request_arguments[arg[0]] = arg[1]
    url = baseUrl + urllib.parse.urlencode(request_arguments)
    if args.verbose:
        logger.info(url)
    pageSoup = bs(requests.get(url, headers=headers).text, features="lxml")
#    if args.verbose:
#        logger.info(pageSoup)

    # Searching links
    for divTag in pageSoup.findAll('div', attrs={'class':'styles_adListItem__3Z_IE'}):  # /!\ Subject to change
        aTags = divTag.findAll('a')
        for aTag in aTags:
            title = aTag.find("p", attrs={"data-qa-id":"aditem_title"}).text
            if aTag.get('href'):
                href = "https://www.leboncoin.fr" + aTag.get('href')
                linkCandidate = SearchLink(href, title)
                if linkCandidate.matches(args):
                    links.append(linkCandidate)
    if args.verbose:
        logger.info("links found: " )
        for link in links:
            logger.info(link)
    return links


def searchesToLinks(searches):
    links = []
    for search in searches:
        for link in searchToLinks(search):
            links.append(link)

    return links

# DB Preparation
db = sqlite3.connect(lbcaDir+"/"+args.db)
db.execute("""
CREATE TABLE IF NOT EXISTS links (
  url TEXT UNIQUE,
  title TEXT DEFAULT "",
  date DATETIME,
  seen BOOL DEFAULT 0,
  nb_views INTEGER,
  emailed BOOL DEFAULT 0
);
""")

gettext.install("lbca")

logger.info("Start !")

if args.gui:
    # GUI mode: We should use a timer and a notification to make it really useful
    import wx
    import gettext

    class LBCMainFrame(wx.Frame):
        def __init__(self, *args, **kwds):
            # begin wxGlade: MyFrame1.__init__

            wx.Frame.__init__(self, *args, **kwds)
            self.itemsList = wx.ListCtrl(self, wx.ID_ANY, style=wx.LC_REPORT | wx.SUNKEN_BORDER)
            self.itemsList.InsertColumn(0, "ID")
            self.itemsList.InsertColumn(1, "URL")
            self.itemsList.InsertColumn(2, "title")
            self.itemsList.InsertColumn(3, "Date")
            self.itemsList.InsertColumn(4, "Seen")
            self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.selectItem, self.itemsList)

            # Menu Bar
            self.generalMenuBar = wx.MenuBar()
            self.General = wx.Menu()
            self.Update = wx.MenuItem(self.General, 2, _("Refresh"), _("Refresh"), wx.ITEM_NORMAL)
            self.General.AppendItem(self.Update)
            self.generalMenuBar.Append(self.General, _("General"))
            self.SetMenuBar(self.generalMenuBar)
            # Menu Bar end

            self.__set_properties()
            self.__do_layout()

            self.Bind(wx.EVT_MENU, self.refreshItems, self.Update)
            # end wxGlade

        def __set_properties(self):
            # begin wxGlade: MyFrame1.__set_properties
            self.SetTitle(_("LBC Alert"))
            self.itemsList.SetMinSize((1000, 740))
            # end wxGlade

        def __do_layout(self):
            # begin wxGlade: MyFrame1.__do_layout
            self.itemsList.SetSize(wx.Size(1000, 740))
            itemsListSize = wx.BoxSizer(wx.VERTICAL)
            itemsListSize.Add(self.itemsList, 1, wx.EXPAND, 0)
            self.SetSizer(itemsListSize)
            itemsListSize.Fit(self)
            self.Layout()
            # end wxGlade

        def loadItems(self, event):
            self.itemsList.DeleteAllItems()
            for row in db.execute("select rowid, url, title, date, seen from links order by seen desc, rowid desc limit 20;"):
                p = self.itemsList.InsertStringItem(0, str(row[0]))
                self.itemsList.SetStringItem(p, 1, row[1])
                self.itemsList.SetStringItem(p, 2, row[2])
                self.itemsList.SetStringItem(p, 3, row[3])
                if row[4]:
                    msg = "Yes"
                else:
                    msg = "No"
                self.itemsList.SetStringItem(p, 4, msg)

        def selectItem(self, event):
            id = event.GetText()
            row = db.execute("select url from links where rowid=?;", (id,)).fetchone()
            os.system("xdg-open "+row[0])
            db.execute("update links set seen=1, nb_views=nb_views+1 where rowid=?;", (id,))
            db.commit()
            event.Skip()
            self.loadItems(event)

        def refreshItems(self, event):
            # We save all links
            for link in searchesToLinks(args.searches):
                db.execute("insert or ignore into links ('url', 'title', 'date') values (?,?,?);", (link.url, link.title, datetime.now()))

            db.commit()
            self.loadItems(event)

    if __name__ == "__main__":
        app = wx.PySimpleApp(0)
        wx.InitAllImageHandlers()
        mainFrame = LBCMainFrame(None, wx.ID_ANY, "")
        app.SetTopWindow(mainFrame)
        mainFrame.Show()
        mainFrame.loadItems(None)
        app.MainLoop()
else:
    # Email mode
    import smtplib
    from email.mime.text import MIMEText
    # We save all links
    links = searchesToLinks(args.searches)
    for link in links:
        db.execute("insert or ignore into links ('url', 'title', 'date') values (?,?,?);", (link.url, link.title, datetime.now()))
    logger.info("found {nb} links".format(nb=len(links)))
    db.commit()

    nb = 0
    text = '<ul>\n'
    for rowid, title, url in db.execute("select rowid, title, url from links where emailed=0;"):
        logger.info("New link found : {link}.".format(link=title))
        text += '<li><a href="{url}">{title} (#{id})</a></li>\n'.format(id=rowid, title=title, url=url)
        db.execute("update links set emailed=1 where rowid=?", (rowid,))
        nb += 1
    text += '</ul>\n'

    if nb == 0:
        logger.info("No new results.")
    elif nb > 0 and args.email_to:
        text = _("found {nb} articles matching your searches:<br />".format(nb=nb)) + '\n' + text
        #smtp = smtplib.SMTP(args.server)
        msg = MIMEText(text, 'html')
        if not args.email_subject:
            msg['Subject'] = "Nouvelles annonces LeBonCoin pour '{search}'".format(search=", ".join(args.searches))
        else:
            msg["subject"] = args.email_subject
        msg['From'] = args.email_from
        msg['To'] = args.email_to
        #smtp.sendmail(msg['From'], [msg['To']], msg.as_string())
        server = smtplib.SMTP(args.server, args.port)
        logger.info("connected to mail server")
        if args.smtpLogin:
            server.starttls()
            server.login(args.smtpLogin, args.smtpPass)
            logger.info("logged in with credentials to mail server")
        server.sendmail(args.email_from, args.email_to, msg.as_string())
        logger.info("Sent email")
        server.quit()
        #logger.info("sending the following email:\n {email}".format(email=msg.as_string()))
        db.commit()
db.close()

logger.info("done.")
