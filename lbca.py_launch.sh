#!/bin/bash

# example use of crontab line
# 59 * * * * /home/ksamak/progra_local/python/leboncoin/lbca.py_launch.sh

cd "$(dirname "$0")";

LOGIN=
PASS=
DEST=
SMTP=smtp.xxx
PORT=587
FROM=
VERBOSE=


./lbca.py -e $DEST -f $FROM --serv $SMTP -p $PORT --login $LOGIN --pass $PASS $VERBOSE\
    -s "example search" \
    -a "price=min-40"

./lbca.py -e $DEST -f $FROM --serv $SMTP -p $PORT --login $LOGIN --pass $PASS $VERBOSE\
	-s "search 1" \
	-s "search 2" \
    -a "price=min-500"


#example search 1
./lbca.py -e $DEST -f $FROM --serv $SMTP -p $PORT --login $LOGIN --pass $PASS $VERBOSE\
    -r Poitiers_86000__46.58387_0.3357_8948_10000 \
    -s "search around poitiers" \
    -s "search 2 around P" \
    --database bd2 \
    --exclude-title not_wanted_word
    --exclude-contents not_wanted_word

#example search 2
./lbca.py -e $DEST -f $FROM --serv $SMTP -p $PORT --login $LOGIN --pass $PASS $VERBOSE\
    -r Poitiers_86000__46.58387_0.3357_8948_10000 \
    -s "terrain" \
    -a "category=9" -a "real_estate_type=3" -a "price=min-100000"

#baseURL to view additional options
# https://www.leboncoin.fr/recherche?category=9&text=terrain&locations=Poitiers_86000__46.58387_0.3357_8948_10000&real_estate_type=3&price=min-100000
